# Acoustic Fire

* Please see the report pdf or docx.

* The code is in the folder titled 'code'. The dataset and its processed variants are large, and not included. Contact us for a copy.

* Notebook run and saved as pdfs are included for convenience.