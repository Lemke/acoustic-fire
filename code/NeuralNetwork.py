import torch
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from collections import OrderedDict


def make_model(num_h_layers, input_size, hidden_size, output_size):
    if num_h_layers > 0 and num_h_layers == len(hidden_size):
        layers = OrderedDict()

        for i in range(num_h_layers):
            if i == 0:
                layer_name = "layer{}".format(i)
                layers[layer_name] = torch.nn.Linear(input_size, hidden_size[0])
                activation_name = "activation{}".format(i)
                layers[activation_name] = torch.nn.ReLU()
            else:
                layer_name = "layer{}".format(i)
                layers[layer_name] = torch.nn.Linear(hidden_size[i - 1], hidden_size[i])
                activation_name = "activation{}".format(i)
                layers[activation_name] = torch.nn.ReLU()

        layers["output"] = torch.nn.Linear(hidden_size[-1], output_size)
        layers["sigmoid"] = torch.nn.Sigmoid()

        return torch.nn.Sequential(layers)
    else:
        raise ValueError("incorrect input")


def train_model(model, optimizer, loss_func, training, validation, batch_size=32, epochs=100):
    trn_loss_track = []
    val_loss_track = []

    for e in range(epochs):
        for i in range(batch_size, len(training[0]), batch_size):
            optimizer.zero_grad()
            output = model(training[0][i - batch_size:i])
            loss = loss_func(output, training[1][i - batch_size:i])
            loss.backward()
            optimizer.step()

        output = model(training[0])
        trn_loss_track.append(float(loss_func(output, training[1]).data))
        output = model(validation[0])
        val_loss_track.append(float(loss_func(output, validation[1]).data))

    return trn_loss_track, val_loss_track


def ploter(trn_loss, val_loss):
    fig = plt.figure()
    x = np.arange(len(trn_loss))
    plt.plot(x, trn_loss, "y", label="training")
    plt.plot(x, val_loss, "b", label="validation")
    plt.title("training and validation losses")
    plt.xlabel("Epochs")
    plt.ylabel("BCE Loss")
    plt.legend()
    plt.show()


def check_accuracy(pred, label):
    # expecting to receive numpy arrays
    pred = (pred > 0.5)
    return (pred == label).sum() / pred.shape[0]

