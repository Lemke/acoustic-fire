import numpy as np
import pandas as pd


def numericalize(labels, categories):
    encoded_pos = []
    i = 0
    for pos in labels:
        location = np.where(categories == pos)
        encoded_pos.append(location[0][0])

    return np.array(encoded_pos)


def normalize(feature):
    normalized = (feature - feature.mean()) / feature.std()
    return normalized


def get_data():
    raw_data = pd.read_excel("Acoustic_Extinguisher_Fire_Dataset.xlsx")

    fuel_names = raw_data['FUEL'].unique()

    test_data = raw_data.sample(frac=0.2)
    train_data = raw_data.drop(test_data.index)

    # Basic prep - turning the fuel types into numbers
    # and normalizing features so that no feature is more important than the others
    # training set
    fuels = numericalize(np.array(train_data['FUEL']), np.array(fuel_names))
    b_prep_trn = train_data.copy()
    b_prep_trn['FUEL'] = fuels
    b_prep_trn['SIZE'] = normalize(b_prep_trn['SIZE'])
    b_prep_trn['DISTANCE'] = normalize(b_prep_trn['DISTANCE'])
    b_prep_trn['DESIBEL'] = normalize(b_prep_trn['DESIBEL'])
    b_prep_trn['AIRFLOW'] = normalize(b_prep_trn['AIRFLOW'])
    b_prep_trn['FREQUENCY'] = normalize(b_prep_trn['FREQUENCY'])
    # test set
    fuels = numericalize(np.array(test_data['FUEL']), np.array(fuel_names))
    b_prep_tst = test_data.copy()
    b_prep_tst['FUEL'] = fuels
    b_prep_tst['SIZE'] = normalize(b_prep_tst['SIZE'])
    b_prep_tst['DISTANCE'] = normalize(b_prep_tst['DISTANCE'])
    b_prep_tst['DESIBEL'] = normalize(b_prep_tst['DESIBEL'])
    b_prep_tst['AIRFLOW'] = normalize(b_prep_tst['AIRFLOW'])
    b_prep_tst['FREQUENCY'] = normalize(b_prep_tst['FREQUENCY'])


    # Encoded Fuels prep - one hot encoding the types of fuels
    # since they have no numerical relation to each other unlike 3 < 4
    # training set
    e_fuels_trn = np.eye(len(fuel_names))[b_prep_trn['FUEL']]
    e_fuels_prep_trn = b_prep_trn.copy()
    e_fuels_prep_trn = e_fuels_prep_trn.drop(columns=['FUEL', 'STATUS'])
    for i in range(len(fuel_names)):
        e_fuels_prep_trn[fuel_names[i]] = e_fuels_trn[:, i]
    e_fuels_prep_trn['STATUS'] = train_data['STATUS']
    # test set
    e_fuels_tst = np.eye(len(fuel_names))[b_prep_tst['FUEL']]
    e_fuels_prep_tst = b_prep_tst.copy()
    e_fuels_prep_tst = e_fuels_prep_tst.drop(columns=['FUEL', 'STATUS'])
    for i in range(len(fuel_names)):
        e_fuels_prep_tst[fuel_names[i]] = e_fuels_tst[:, i]
    e_fuels_prep_tst['STATUS'] = test_data['STATUS']

    # they need shuffled so that the order of experiments doesn't affect out predictions
    b_prep_trn['FUEL'] = normalize(b_prep_trn['FUEL'])
    b_prep_trn = np.array(b_prep_trn)
    np.random.shuffle(b_prep_trn)
    basic_trn = (b_prep_trn[:, :-1], np.expand_dims(b_prep_trn[:, -1], axis=1))

    b_prep_tst['FUEL'] = normalize(b_prep_tst['FUEL'])
    b_prep_tst = np.array(b_prep_tst)
    np.random.shuffle(b_prep_tst)
    basic_tst = (b_prep_tst[:, :-1], np.expand_dims(b_prep_tst[:, -1], axis=1))

    e_fuels_prep_trn = np.array(e_fuels_prep_trn)
    np.random.shuffle(e_fuels_prep_trn)
    encoded_trn = (e_fuels_prep_trn[:, :-1], np.expand_dims(e_fuels_prep_trn[:, -1], axis=1))

    e_fuels_prep_tst = np.array(e_fuels_prep_tst)
    np.random.shuffle(e_fuels_prep_tst)
    encoded_tst = (e_fuels_prep_tst[:, :-1], np.expand_dims(e_fuels_prep_tst[:, -1], axis=1))

    return (basic_trn, basic_tst), (encoded_trn, encoded_tst)
